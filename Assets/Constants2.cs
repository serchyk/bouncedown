﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class Constants2 : MonoBehaviour {

	public int shieldsCount;
	public int damagesCount;
	public int useShields;
	public int useDamages;
	int forCheckD;
	int forCheckS;

	//public string appID="";
	//public string bannerID="";
	//public string interstitialID="";
	//public string videoID="";
	//public string nativeBannerID = "";
	//public BannerView bannerView;

	//public Slider sliderDamage;
	public Text textUseCountD;
	//public Slider sliderShield;
	public Text textUseCountS;

	public void Start(){

		//#if UNITY_IOS
		//appID="ca-app-pub-3940256099942544~1458002511";
		//bannerID="ca-app-pub-3940256099942544/2934735716";
		//interstitialID="ca-app-pub-3940256099942544/4411468910";
		//videoID="ca-app-pub-3940256099942544/1712485313";
		//nativeBannerID = "ca-app-pub-3940256099942544/3986624511";
		//#elif UNITY_ANDROID
		//appID="ca-app-pub-3940256099942544~3347511713";
		//bannerID="ca-app-pub-3940256099942544/6300978111";
		//interstitialID="ca-app-pub-3940256099942544/1033173712";
		//videoID="ca-app-pub-3940256099942544/5224354917";
		//nativeBannerID = "ca-app-pub-3940256099942544/2247696110";
		//#endif


		// Initialize the Google Mobile Ads SDK.
		//MobileAds.Initialize(appID);

		shieldsCount = PlayerPrefs.GetInt ("buffShield");
		damagesCount = PlayerPrefs.GetInt ("buffDamage");
		//useShields = PlayerPrefs.GetInt ("useShields");
		//useDamages = PlayerPrefs.GetInt ("useDamages");
		if(textUseCountD != null){
			
		}
		if(textUseCountS != null){
			
		}
		forCheckD = PlayerPrefs.GetInt ("buffDamage");
		forCheckS = PlayerPrefs.GetInt ("buffShield");

		//// Create a 320x50 banner at the top of the screen.
		//bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Bottom);

		//// Create an empty ad request.
		//AdRequest request = new AdRequest.Builder().Build();

		//// Load the banner with the request.
		//bannerView.LoadAd(request);
	}

	public void Update()
	{

		shieldsCount = PlayerPrefs.GetInt ("buffShield");
		damagesCount = PlayerPrefs.GetInt ("buffDamage");

		if(textUseCountD != null){
			textUseCountD.text = useDamages.ToString() + "/" + forCheckD.ToString();

			//useDamages = (int)sliderDamage.value;
		}
		if(textUseCountS != null){
			textUseCountS.text = useShields.ToString() + "/" + forCheckS.ToString();

			//useShields = (int)sliderShield.value;
		}
	}
	public void nextBD(){
		if(useDamages <  forCheckD)
		useDamages += 1;
	}
	public void previewBD(){
		if(useDamages > 0)
			useDamages -= 1;
	}

	public void nextBS(){
		if(useShields <  forCheckS)
			useShields += 1;
	}
	public void previewBS(){
		if(useShields > 0)
			useShields -= 1;
	}

	public void DebugBuffs(){
		PlayerPrefs.SetInt ("useShields",0);
		PlayerPrefs.SetInt ("useDamages",0);
		PlayerPrefs.SetInt ("buffShield",0);
		PlayerPrefs.SetInt ("buffDamage",0);
	}
}