﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class delObject : MonoBehaviour {

    public List<GameObject> objDel;
    Animator anim;
    public GameObject animator;
    public float delTime = 2f;
    public GameObject getTransform;
    public GameObject cam;

    public void Start()
    {
        cam = GameObject.Find("Main Camera");
        //anim = gameObject.GetComponent<Animator>();
        animator = GetComponentInParent<Animator>().gameObject;

        
    }

    private void Update()
    {
        if (delTime <= 0) {
            for (int i = 0; i < objDel.Count; i++)
            {
                Destroy(animator);
            }
        }
        if (delTime <= 1f && delTime != 0f)
        {
            delTime -= Time.deltaTime;
            animator.GetComponent<Animator>().SetBool("destr", true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "player" && animator != null)
        {
           
            delTime = 0.3f;
                animator.GetComponent<Animator>().SetBool("destr", true);
            for (int i = 0; i < objDel.Count; i++)
            {
                objDel[i].GetComponent<MeshCollider>().enabled = false;
				if (objDel [i].GetComponentInChildren<Animator> () != null) {
					objDel [i].GetComponentInChildren<Animator> ().enabled = false;
					objDel [i].GetComponentInChildren<Animator> ().gameObject.GetComponent<MeshCollider>().enabled = false;
				}
            }
        }
        
    }
    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.GetComponent<gravitySuet>().comboX > 2 && gameObject.tag == "tableObj")
        {

            
            other.gameObject.GetComponent<gravitySuet>().comboX = 0;
            animator.GetComponent<delObject>().delTime = 1f;
            animator.GetComponent<delObject>().getTransform = other.gameObject;
            for (int i = 0; i < animator.GetComponent<delObject>().objDel.Count; i++)
            {
                animator.GetComponent<delObject>().objDel[i].GetComponent<MeshCollider>().enabled = false;
            }
            Debug.Log(other.gameObject.GetComponent<gravitySuet>().maxY);
            
        }
    }
 }
