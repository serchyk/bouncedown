﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour {

	Constants2 CS2;
	public rotateController rC;
    GameObject startGame;
    public GameObject loseGame;
    GameObject wonGame;
    GameObject playGame;
    JSONParser jParser;
    public Image progressBar;
    public gravitySuet gS;
    public float nextStageCount;
    public GameObject oNL;
    public GameObject won;
    public int nEw;
    public Text level;


    public Text wonScore;
    public Text wonRecord;
	public List<GameObject> nextLevelGO;

	public List<Text> textD;
	public List<Text> textS;
	public int scenePreview;
	public bool isDead;

	public List<GameObject> panelBuffs;

    // Use this for initialization
    void Start () {
        startGame = GameObject.FindGameObjectWithTag("startScene");
        loseGame = GameObject.FindGameObjectWithTag("loseScene");
        wonGame = GameObject.FindGameObjectWithTag("wonScene");
        playGame = GameObject.FindGameObjectWithTag("playScene");



        jParser = FindObjectOfType<JSONParser>();
		CS2 = FindObjectOfType<Constants2>();
		gS = FindObjectOfType<gravitySuet>();

        loseGame.SetActive(false);
        wonGame.SetActive(false);
        playGame.SetActive(false);
		Text buffLevel = gameObject.GetComponentInChildren<Transform> ().Find ("cStart").Find ("Panel").GetComponentInChildren<Transform> ().Find ("upBuffs").GetComponentInChildren<Transform> ().Find ("Level").gameObject.GetComponent<Text> ();
		buffLevel.text = "Level № " + (PlayerPrefs.GetInt("levelSelect") + 1).ToString ();
		startGame.GetComponentInChildren<Transform>().Find("Panel").GetComponentInChildren<Transform>().Find("upBuffs").GetComponentInChildren<Transform>().Find("maxScore").GetComponent<Text>().text = "Record:" + PlayerPrefs.GetInt("CountForLevel" + PlayerPrefs.GetInt("levelSelect")).ToString();
    }
	
	// Update is called once per frame
	void Update () {
		gS = FindObjectOfType<gravitySuet>();
        rC = FindObjectOfType<rotateController>();
        if(jParser != null)
        progressBar.fillAmount = nextStageCount/ (jParser.levelload.leveles.Count - 2);
		if (scenePreview == 0) {
			textD[0].text = CS2.useDamages.ToString ();
			textS[0].text = CS2.useShields.ToString ();
		}
		if (scenePreview == 1) {
			textD[1].text = CS2.useDamages.ToString ();
			textS[1].text = CS2.useShields.ToString ();
		}
		if (isDead) {
			rC.enabled = false;
		}
    }

    public void startScene() {
		for (int jk = 0; jk < panelBuffs.Count; jk++) {
			if (jk == 0) {
				rC.enabled = true;
				panelBuffs [jk].SetActive (false);
			}
			if (jk == 1) {
				scenePreview = 1;
				panelBuffs [jk].SetActive (true);
			}
		}

        startGame.SetActive(false);
        playGame.SetActive(true);
    }
    public void loseScene() {
        playGame.SetActive(false);
        loseGame.SetActive(true);
        
        rC.enabled = false;
        loseGame.GetComponentInChildren<Transform>().Find("maxScore").GetComponent<Text>().text = "Record:" + PlayerPrefs.GetInt("CountForLevel" + PlayerPrefs.GetInt("levelSelect")).ToString();
        loseGame.GetComponentInChildren<Transform>().Find("score").GetComponent<Text>().text = playGame.GetComponentInChildren<Transform>().Find("score").GetComponent<Text>().text;
        playGame.GetComponentInChildren<Transform>().Find("score").GetComponent<Text>().text = "";
    }
    public void playScene(string score) {
        playGame.GetComponentInChildren<Transform>().Find("score").GetComponent<Text>().text = score;
    }
    public void wonScene()
    {
		if (PlayerPrefs.GetInt ("levelCount") - 1 == jParser.levelid) {
			for (int i = 0; i < nextLevelGO.Count; i++) {
				nextLevelGO [i].SetActive (false);
			}
		}
        playGame.SetActive(false);
        wonGame.SetActive(true);
        rC.enabled = false;
        if (nEw == 1)
        {
            oNL.SetActive(true);

        }
        else
        {
            won.SetActive(true);
            wonRecord.text = "Record:" + PlayerPrefs.GetInt("CountForLevel" + PlayerPrefs.GetInt("levelSelect")).ToString();
            wonScore.text = playGame.GetComponentInChildren<Transform>().Find("score").GetComponent<Text>().text;
        }


        if (nEw == 1) {
            oNL.SetActive(true);
        }

    }
    public void restartScene() {
        SceneManager.LoadScene(1);
    }
    public void mainmenuScene()
    {
        SceneManager.LoadScene(0);
    }
    public void nextLevel()
    {
        PlayerPrefs.SetInt("levelSelect", jParser.levelid + 1);
        SceneManager.LoadScene(1);
    }
    public void nextOk()
    {
        oNL.SetActive(false);
        won.SetActive(true);
        wonRecord.text = "Record:" + PlayerPrefs.GetInt("CountForLevel" + PlayerPrefs.GetInt("levelSelect")).ToString();
        wonScore.text = playGame.GetComponentInChildren<Transform>().Find("score").GetComponent<Text>().text;
    }

	public void startBuffGame(){
		for (int jk = 0; jk < panelBuffs.Count; jk++) {
			if (jk == 0) {
				panelBuffs [jk].SetActive (true);
				scenePreview = 0;
			}
		}
		GameObject buff = gameObject.GetComponentInChildren<Transform> ().Find ("cStart").GetComponentInChildren<Transform> ().Find ("Panel").gameObject;
		buff.SetActive (false);
		rC.enabled = true;
	}

	public void activityDamage(){
		if (CS2.useDamages > 0 && !gS.isDamage) {
			CS2.useDamages -= 1;
			gS.isDamage = true;
		}
	}
	public void activityShield(){
		if (CS2.useShields > 0 && !gS.isShield) {
			CS2.useShields -= 1;
			gS.isShield = true;
		}
	}
}
