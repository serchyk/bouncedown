﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggleScript : MonoBehaviour {

    public Image toggleBackground;
    public Toggle tuggleButton;

    private void Start()
    {
		if (PlayerPrefs.GetInt("FIRSTTIMEOPENING", 1) == 1)
		{
			PlayerPrefs.SetInt("sound", 1);
			PlayerPrefs.SetInt("FIRSTTIMEOPENING", 0);
		}
		else
		{
			//Sound
			if (PlayerPrefs.GetInt ("sound") == 1) {
				//tuggleSoundButton.isOn = false;
				tuggleButton.isOn = false;
				toggleBackground.enabled = true;
			} else {
				PlayerPrefs.SetInt("sound", 0);
				tuggleButton.isOn = true;
				toggleBackground.enabled = false;
			}
		}
		Debug.Log (PlayerPrefs.GetInt ("sound"));
           
    }

    // Update is called once per frame
    void Update () {
        if (!tuggleButton.isOn)
        {
            PlayerPrefs.SetInt("sound", 1);
            toggleBackground.enabled = true;
        }
        else
        {
            PlayerPrefs.SetInt("sound", 0);
            toggleBackground.enabled = false;
        }
    }


}
