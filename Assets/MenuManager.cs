﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class MenuManager : MonoBehaviour {

    public int numberSceneGame;
    public GameObject menu;
    public GameObject levels;
	public GameObject giftP;
	public GameObject giftB;
	public GameObject loadVideo;
	private RewardBasedVideoAd rewardBasedVideo;
    public GameObject damageSprite;
    public GameObject shieldSprite;

    public string appID = "";
    public string bannerID = "";
    public string interstitialID = "";
    public string videoID = "";
    public string nativeBannerID = "";


	public void Start()
	{
        #if UNITY_IOS
        appID="ca-app-pub-3940256099942544~1458002511";
        bannerID="ca-app-pub-3940256099942544/2934735716";
        interstitialID="ca-app-pub-3940256099942544/4411468910";
        videoID="ca-app-pub-3940256099942544/1712485313";
        nativeBannerID = "ca-app-pub-3940256099942544/3986624511";
        #elif UNITY_ANDROID
        appID = "ca-app-pub-3940256099942544~3347511713";
        bannerID = "ca-app-pub-3940256099942544/6300978111";
        interstitialID = "ca-app-pub-3940256099942544/1033173712";
        videoID = "ca-app-pub-3940256099942544/5224354917";
        nativeBannerID = "ca-app-pub-3940256099942544/2247696110";
        #endif


        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appID);
	}

	public void OnLevelLoaded(int level)
    {
        PlayerPrefs.SetInt("levelSelect", level);
        SceneManager.LoadScene(numberSceneGame);
    }

    public void levelOpen()
    {
        menu.SetActive(false);
        levels.SetActive(true);
    }

    public void exitGame()
    {
        Application.Quit();
    }
    public void previu() {
        menu.SetActive(true);
        levels.SetActive(false);
    }

	public void openGift(){
		giftB.SetActive (false);
		giftP.SetActive (true);

        loadVideo.SetActive(true);

        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        this.RequestRewardBasedVideo();
        // Called when an ad request has successfully loaded.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
	}

    private void RequestRewardBasedVideo()
    {

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, videoID);

    }

	public void exitGift(){
        damageSprite.SetActive(false);
        shieldSprite.SetActive(false);
		giftB.SetActive (true);

        giftP.SetActive (false);
	}

    public void HandleRewardBasedVideoLoaded(object sender, System.EventArgs args)
    {
        rewardBasedVideo.Show();
        Debug.Log("loaded");
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        loadVideo.SetActive(false);
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
            + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, System.EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, System.EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, System.EventArgs args)
    {
        loadVideo.SetActive(false);
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        loadVideo.SetActive(false);
        int addGift = Random.Range(0, 2);
        if(addGift == 0){
            damageSprite.SetActive(true);
            PlayerPrefs.SetInt("buffDamage", PlayerPrefs.GetInt("buffDamage") + 1);
        }

        if (addGift == 1)
        {
            shieldSprite.SetActive(true);
            PlayerPrefs.SetInt("buffShield", PlayerPrefs.GetInt("buffShield") + 1);
        }
        //string type = args.Type;
        //double amount = args.Amount;
        //MonoBehaviour.print(
        //  "HandleRewardBasedVideoRewarded event received for "
        //  + amount.ToString() + " " + type);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, System.EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }
}
