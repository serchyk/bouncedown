using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

public class JSONParser : MonoBehaviour {

    public int levelid;
    public MaterialList newMaterial;
    public LevelList levelload;
    public GameObject backColor;
    public Color32 tableCol;
    public Color32 ballCol;
    public Color32 enemyCol;
    public Color32 cylinderCol;
    public Color32 backCol;

    public Gradient grad = new Gradient();

    string json;
    GameObject previuObj;
    GameObject variableBall;
    public int allStages;
    public Color32 test;
    public float test2;

    GradientColorKey[] colorKeys = new GradientColorKey[3];
    GradientAlphaKey[] alphaKeys = new GradientAlphaKey[3];

    // Use this for initialization
    void Start () {

        

        levelid = PlayerPrefs.GetInt("levelSelect");
        readLevel();
        
        //new Color32((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b, (byte)newMaterial.materials[0].tableColor[0].a);
        ballCol = new Color32((byte)newMaterial.materials[0].ballColor[0].r, (byte)newMaterial.materials[0].ballColor[0].g, (byte)newMaterial.materials[0].ballColor[0].b, (byte)newMaterial.materials[0].ballColor[0].a);
        cylinderCol = new Color32((byte)newMaterial.materials[0].cylinderColor[0].r, (byte)newMaterial.materials[0].cylinderColor[0].g, (byte)newMaterial.materials[0].cylinderColor[0].b, 1);
        backCol = new Color32((byte)newMaterial.materials[0].tableColor[0].r, (byte)(newMaterial.materials[0].tableColor[0].g - 40), (byte)newMaterial.materials[0].tableColor[0].b, 1);
        enemyCol = new Color32((byte)newMaterial.materials[0].enemyColor[0].r, (byte)newMaterial.materials[0].enemyColor[0].g, (byte)newMaterial.materials[0].enemyColor[0].b, 1);
        tableCol = new Color32((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b, 1);

        //Material for Enemy
        Material matEnemy = Resources.Load<Material>("mEnemy");
        matEnemy.SetColor("_Color", enemyCol);
        //Material for Table
        Material matTable = Resources.Load<Material>("mTable");
        matTable.SetColor("_Color", tableCol);
        //Material for Cylinder
        Material matCylinder = Resources.Load<Material>("mCylinder");
        matCylinder.SetColor("_Color", cylinderCol);

        Debug.Log(PlayerPrefs.GetInt("levelLock" + levelid));
        for (int i = 0; i < levelload.leveles.Count; i++)
        {
            
            GameObject prefabLevel = Instantiate(Resources.Load<GameObject>(levelload.leveles[i].Name));
            prefabLevel.GetComponent<Renderer>().material = matCylinder;

            if (i == 0)
            {
                prefabLevel.name = (levelload.leveles[i].Name + levelload.leveles[i].id);
                prefabLevel.transform.position = new Vector3(2.0000000072549876e-15f, 3.3f, -7.302999973297119f);
                GameObject Ball = Instantiate(Resources.Load<GameObject>("Sphere"));
                
               
                

                Ball.name = "Player";
                Ball.transform.position = new Vector3(0f, 4.3f, -9f);
                Ball.GetComponent<Renderer>().material.SetColor("_Color", new Color32((byte)newMaterial.materials[0].ballColor[0].r, (byte)newMaterial.materials[0].ballColor[0].g, (byte)newMaterial.materials[0].ballColor[0].b, (byte)255f));
                //Debug.Log(new Color32((byte)newMaterial.materials[0].ballColor[0].r, (byte)newMaterial.materials[0].ballColor[0].g, (byte)newMaterial.materials[0].ballColor[0].b, (byte)newMaterial.materials[0].ballColor[0].a));
                backColor.GetComponent<Renderer>().material.SetColor("_Color", backCol);
                //Tttt
                alphaKeys[0].time = 0;
                alphaKeys[0].alpha = 1;

                alphaKeys[1].time = 0.70f;
                alphaKeys[1].alpha = 1;

                alphaKeys[2].time = 1;
                alphaKeys[2].alpha = 0;

                colorKeys[0].time = 0f;
                colorKeys[0].color = new Color32((byte)newMaterial.materials[0].ballColor[0].r, (byte)(newMaterial.materials[0].ballColor[0].g), (byte)newMaterial.materials[0].ballColor[0].b, (byte)newMaterial.materials[0].ballColor[0].a);

                colorKeys[1].time = 0.54f;
                colorKeys[1].color = new Color32((byte)newMaterial.materials[0].ballColor[0].r, (byte)(newMaterial.materials[0].ballColor[0].g), (byte)newMaterial.materials[0].ballColor[0].b, (byte)newMaterial.materials[0].ballColor[0].a);


                colorKeys[2].time = 1;
                colorKeys[2].color = new Color32((byte)newMaterial.materials[0].ballColor[0].r, (byte)(newMaterial.materials[0].ballColor[0].g), (byte)newMaterial.materials[0].cylinderColor[0].b, (byte)newMaterial.materials[0].ballColor[0].a);

                grad.SetKeys(colorKeys, alphaKeys);
                ParticleSystem ps = Ball.GetComponent<ParticleSystem>();
                //Ball.GetComponent<ParticleSystem>().colorOverLifetime.enabled = true;
                var col = ps.colorOverLifetime;
                col.enabled = true;
                col.color = grad;
                Debug.Log(newMaterial.materials[0].ballColor);
                variableBall = Ball;

            }
            else
            {
                previuObj = GameObject.Find(levelload.leveles[i - 1].Name + levelload.leveles[i - 1].id);
                prefabLevel.name = (levelload.leveles[i].Name + levelload.leveles[i].id);
                prefabLevel.transform.position = new Vector3(previuObj.transform.position.x, previuObj.transform.position.y - 2.5f, previuObj.transform.position.z);
                prefabLevel.transform.SetParent(previuObj.transform);
            }

            for (int j = 0; j < prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel.Count; j++)
            {
                GameObject prefab = Instantiate(Resources.Load<GameObject>(levelload.leveles[i].platforms[j].Name));
				prefab.name = "id:" + levelload.leveles[i].platforms[j].id +"Name:"+ levelload.leveles[i].platforms[j].Name;
                GameObject platforms = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel[j].gameObject;
                if (levelload.leveles[i].platforms[j].Name == "table") {
                    platforms.transform.gameObject.tag = "tableObj";
                    platforms.transform.gameObject.GetComponent<MeshCollider>().enabled = true;
                    //prefab.GetComponent<Renderer>().material.n
                    //Debug.Log(new Color32((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b, (byte)newMaterial.materials[0].tableColor[0].a));
                    
                    //prefab.GetComponentInParent<Renderer>().material.SetColor("_Color", new Color((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b));
                    prefab.GetComponent<Renderer>().material = matTable;
                    //platforms.GetComponent<Renderer>().material.SetColor("_Color", tableCol);
                    //platforms.GetComponent<MeshRenderer>().enabled = true;
                    //Debug.Log(prefab.GetComponent<Renderer>().material.GetColor(i));
                    platforms.GetComponent<delObject>().enabled = true;
                    prefab.GetComponent<delObject>().enabled = true;
                    platforms.gameObject.GetComponent<delObject>().objDel = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel;
                }

				if (levelload.leveles[i].platforms[j].Name == "buffShield") {
					platforms.transform.gameObject.tag = "tableObj";
					//prefab.GetComponent<Renderer>().material.n
					//Debug.Log(new Color32((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b, (byte)newMaterial.materials[0].tableColor[0].a));

					//prefab.GetComponentInParent<Renderer>().material.SetColor("_Color", new Color((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b));
					prefab.GetComponent<Renderer>().material = matTable;
					//platforms.GetComponent<Renderer>().material.SetColor("_Color", tableCol);
					//platforms.GetComponent<MeshRenderer>().enabled = true;
					//Debug.Log(prefab.GetComponent<Renderer>().material.GetColor(i));
					platforms.GetComponent<delObject>().enabled = true;
					prefab.GetComponent<delObject>().enabled = true;
					platforms.gameObject.GetComponent<delObject>().objDel = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel;
				}

				if (levelload.leveles[i].platforms[j].Name == "buffDamage") {
					platforms.transform.gameObject.tag = "tableObj";
					//prefab.GetComponent<Renderer>().material.n
					//Debug.Log(new Color32((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b, (byte)newMaterial.materials[0].tableColor[0].a));

					//prefab.GetComponentInParent<Renderer>().material.SetColor("_Color", new Color((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b));
					prefab.GetComponent<Renderer>().material = matTable;
					//platforms.GetComponent<Renderer>().material.SetColor("_Color", tableCol);
					//platforms.GetComponent<MeshRenderer>().enabled = true;
					//Debug.Log(prefab.GetComponent<Renderer>().material.GetColor(i));
					platforms.GetComponent<delObject>().enabled = true;
					prefab.GetComponent<delObject>().enabled = true;
					platforms.gameObject.GetComponent<delObject>().objDel = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel;
				}

                if (levelload.leveles[i].platforms[j].Name == "enemy")
                {
                    //prefab.GetComponent<Renderer>().material.SetColor("_Color", enemyCol);
                    prefab.GetComponent<Renderer>().material = matEnemy;
                    platforms.transform.gameObject.tag = "enemy";
					platforms.GetComponent<delObject>().enabled = true;
					prefab.GetComponent<delObject>().enabled = true;
					platforms.gameObject.GetComponent<delObject>().objDel = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel;
                }

				if (levelload.leveles[i].platforms[j].Name == "enemyWalking")
				{
					//prefab.GetComponent<Renderer>().material.SetColor("_Color", enemyCol);
					prefab.GetComponentInChildren<Renderer>().material = matEnemy;
					prefab.GetComponentInChildren<delObject>().objDel = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel;
					prefab.GetComponentInChildren<delObject>().objDel [j] = prefab.GetComponentInChildren<Animator> ().gameObject;
					Debug.Log (prefab.GetComponentInChildren<Animator> ().gameObject.name);
				}

                if (levelload.leveles[i].platforms[j].Name == "next") {
                    prefab.gameObject.GetComponent<delObject>().objDel = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel;
                    platforms.GetComponent<delObject>().enabled = true;
                    platforms.gameObject.GetComponent<delObject>().objDel = prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel;
                    platforms.transform.gameObject.tag = "nextStage";
                    
                    //Destroy(prefab);
                }
				if (levelload.leveles[i].platforms[j].Name == "tableEnemy")
				{
					//prefab.GetComponent<Renderer>().material.SetColor("_Color", enemyCol);
					prefab.GetComponent<Renderer>().material = matTable;
					prefab.GetComponentInChildren<Transform>().transform.gameObject.tag = "enemy";
					prefab.GetComponentInChildren<Renderer> ().material = matEnemy;
				}
                platforms.name = prefab.name;
                prefab.transform.position = platforms.transform.position;
                //prefab.transform.SetParent(prefabLevel.transform.Find("GameObject").transform);
                prefabLevel.transform.Find("GameObject").GetComponent<delObject>().objDel[j] = prefab;
                prefab.transform.rotation = Quaternion.Euler(platforms.transform.eulerAngles.x, platforms.transform.eulerAngles.y, platforms.transform.eulerAngles.z);
                prefab.transform.SetParent(platforms.transform);
            }
        }
        allStages = levelload.leveles.Count;
    }
    private void Update()
    {
        test = new Color32((byte)newMaterial.materials[0].tableColor[0].r, (byte)newMaterial.materials[0].tableColor[0].g, (byte)newMaterial.materials[0].tableColor[0].b, (byte)newMaterial.materials[0].tableColor[0].a);
    }
    //Считываем данные level с json
    void readLevel() {
        //Разбираем
        string path = Path.Combine(Application.streamingAssetsPath, "level" + levelid + ".json");
        WWW reader = new WWW(path);
        while (!reader.isDone) { }
        json = reader.text;//читаем
        newMaterial = JsonUtility.FromJson<MaterialList>(json);
        levelload = JsonUtility.FromJson<LevelList>(json);//загружаем основу
        


    }

    //Создание Основы
    [System.Serializable]
    public class levels
    {
        public int id;
        public string Name;
        public List<platform> platforms;
    }
    [System.Serializable]
    public class materiales
    {
        public List<cylinderC> cylinderColor;
        public List<tableC> tableColor;
        public List<ballC> ballColor;
        public List<enemyC> enemyColor;
    }

    [System.Serializable]
    public class platform
    {
        public int id;
        public string Name;
    }
    [System.Serializable]
    public class LevelList
    {
        public List<levels> leveles;
    }

    [System.Serializable]
    public class cylinderC
    {
        public float r;
        public float g;
        public float b;
        public float a;
    }
    [System.Serializable]
    public class tableC
    {
        public float r;
        public float g;
        public float b;
        public float a;
    }
    [System.Serializable]
    public class ballC
    {
        public float r;
        public float g;
        public float b;
        public float a;
    }
    [System.Serializable]
    public class enemyC
    {
        public float r;
        public float g;
        public float b;
        public float a;
    }

    [System.Serializable]
    public class MaterialList
    {
        public List<materiales> materials;
    }
    //*****************************
}
