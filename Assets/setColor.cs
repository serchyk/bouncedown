﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setColor : MonoBehaviour {

    public GameObject objColor;

	// Use this for initialization
	void Start () {
        objColor = GameObject.Find("CylinderStart0");
        gameObject.GetComponent<Renderer>().material.color = objColor.GetComponent<Renderer>().material.color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
