﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class AHSave : MonoBehaviour
{

    public Vector3 positions;

    private Save sv = new Save();
    private string path;

    private void Update()
    {
        positions = transform.position;
    }

    public void Saves()
    {
        path = Path.Combine(Application.dataPath, "Save.json");
        if (File.Exists(path))
        {
            sv = JsonUtility.FromJson<Save>(File.ReadAllText(path));
            sv.Name = gameObject.name;
            sv.pos = positions;
            Debug.Log(sv.Name +", "+sv.pos);
        }
        File.WriteAllText(path, JsonUtility.ToJson(sv));
    }
}
[Serializable]
public class Save
{
    public string Name;
    public Vector3 pos;
}