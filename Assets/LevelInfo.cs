﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelInfo : MonoBehaviour {
    public int id;
    public int count;
    public bool Unlock;
    public Image lockImage;
    public Text nameLevel;
    public Button startGame;
    public List<Image> star;
    public GameObject previuLevel;
    //90 за звезду

    public void OnLevelLoaded()
    {
        PlayerPrefs.SetInt("levelSelect", id);
        SceneManager.LoadScene(1);
		PlayerPrefs.SetInt ("MENU", 0);
    }
}
