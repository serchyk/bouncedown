﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gravitySuet : MonoBehaviour {

	Constants CS;
	Constants2 CS2;
    Rigidbody control;
    public bool isGround;
	Vector3 moveVector;
    public bool isNullObject;
    public bool isStop;
    public rotateController rc;
    GameObject cam;
    public float camY;
    public bool isCam;
    public float maxY;
    public float speed;
    public int comboX;
    float camSpeedNormal;
    public float fallSpeed;
    public int count;
    GameSceneManager gSM;
    public Transform splash;
    public RaycastHit hit;
    float range = 5f;
    public float randomRot;
    public GameObject parent;
    JSONParser jPars;
    public int Lock;
    public List<AudioClip> sounds;
    public AudioClip clip;
    public AudioSource source;
	bool clearBuffs = false;

	public float jumpTimer;
	public GameObject shield;
	public bool isShield = false;

	public GameObject damage;
	public bool isDamage = false;
	public bool isBuffs = false;
	public float buffTimer;
	public bool isJump;



    private void Start()
    {
        gSM = FindObjectOfType<GameSceneManager>();
		//CS = FindObjectOfType<Constants>();
		CS2 = FindObjectOfType<Constants2>();
        camSpeedNormal = speed;
        control = GetComponent<Rigidbody>();
        cam = GameObject.Find("Main Camera");
        jPars = FindObjectOfType<JSONParser>();
        rc = FindObjectOfType<rotateController>();
        // PlayerPrefs.GetInt("levelLock" + jPars.levelid + 1);
		//controler = GetComponent<CharacterController> ();

        source = GetComponent<AudioSource>();
        source.playOnAwake = false;
        source.mute = false;
        source.loop = false;
		isShield = false;
		isDamage = false;
		isBuffs = false;
    }

    public void Update()
	{
		/*isGround = IsGrounded ();
		if (isGround) {
			verticalVelocity = jumpForce;
			transform.position = new Vector3 (transform.position.x, verticalVelocity, transform.position.z);
			verticalVelocity += (gravity * Time.deltaTime);
		} else {
			transform.position = new Vector3 (transform.position.x, verticalVelocity, transform.position.z);
			verticalVelocity -= (gravity * Time.deltaTime);
		}*/

		//moveVector.y = verticalVelocity;

        gameObject.transform.rotation = Quaternion.Euler(90f,0f,0f);
        randomRot = Random.Range(0, 360);

		if (isShield) {
			shield.SetActive (true);
		} else {
			shield.SetActive (false);
		}
		if (isBuffs && !isDamage && buffTimer > 0) {
				buffTimer -= Time.deltaTime;
		}
		if (buffTimer < 0) {
			isBuffs = false;
		}
		if (isDamage) {
			damage.SetActive (true);
			isBuffs = true;
		} else {
			damage.SetActive (false);
		}
        
        if (!isNullObject)
        {
			if (!isStop) {
				if (!isBuffs) {
					
						//transform.position = new Vector3 (transform.position.x, 6f - 0.2f, transform.position.z);
						Jump ();
					if (jumpTimer > 0) {
						jumpTimer -= Time.deltaTime;
					}
						//isJump = false;
				}
			}
        }
        if(isCam && !isStop)
        {
            cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y - speed, cam.transform.position.z);
            if (cam.transform.position.y < maxY + 0.5f)
            {
                cam.transform.position = new Vector3(cam.transform.position.x, maxY+0.5f, cam.transform.position.z);
                isCam = false;
            }
        }
        if (gSM.nextStageCount == 1) {
			if (!clearBuffs) {
				PlayerPrefs.SetInt ("buffDamage", PlayerPrefs.GetInt ("buffDamage") - CS2.useDamages);
				PlayerPrefs.SetInt ("buffShield", PlayerPrefs.GetInt ("buffShield") - CS2.useShields);
				clearBuffs = true;
			}
            gSM.startScene();
        }
    }

    public void Jump() {
			if (isGround) {
				control.AddForce (gameObject.transform.position.x, fallSpeed, gameObject.transform.position.z, ForceMode.Impulse);
				isGround = false;
			}
		//isGround = IsGrounded();

    }

    public void OnCollisionEnter(Collision collision)
    {


        if (Physics.Raycast(transform.position, transform.forward, out hit, range))
        {
            //Debug.Log(hit.transform.gameObject.name);
            parent = hit.transform.gameObject;
			if(splash != null){
            GameObject splashe = Instantiate(Resources.Load<GameObject>("splashing"));
            GameObject part = Instantiate(Resources.Load<GameObject>("part"));

            part.transform.position = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z);
            //part.transform.rotation = Quaternion.Euler(90f, randomRot, 0f);
            part.GetComponent<Renderer>().material.color = gameObject.GetComponent<Renderer>().material.color;
            splashe.transform.position = new Vector3(transform.position.x, transform.position.y - 0.1f, transform.position.z);
            splashe.transform.rotation = Quaternion.Euler(90f, randomRot, 0f);
            splashe.GetComponent<Renderer>().material.color = gameObject.GetComponent<Renderer>().material.color;//
            //splashe.GetComponent<colorForShpleg>().test = splashe.GetComponentInChildren<Transform>().gameObject.transform.Find("Quad").gameObject;
            splashe.GetComponentInChildren<Transform>().gameObject.transform.Find("Quad").gameObject.GetComponent<Renderer>().material.color = new Color(gameObject.GetComponent<Renderer>().material.color.r, gameObject.GetComponent<Renderer>().material.color.g, gameObject.GetComponent<Renderer>().material.color.b, 255);
            //splashe.GetComponentInChildren<colorForShpleg>().test.GetComponent<Renderer>().material.color = gameObject.GetComponent<Renderer>().material.color;
            splashe.transform.SetParent(parent.transform);
            //splashe = gameObject.GetComponent<Renderer>().material.color;
			}
        }
		if (isDamage) {
			
			gSM.nextStageCount += 1;
			comboX++;
			speed += 0.1f;
			maxY = transform.position.y - camY;
			isCam = true;

			count += 10;
			gSM.playScene(count.ToString());
			collision.gameObject.GetComponent<MeshCollider>().enabled = false;
			collision.gameObject.GetComponentInParent<delObject>().animator.GetComponent<delObject>().delTime = 0.3f;
			isDamage = false;
			buffTimer = 0.3f;

		}
        if (collision.gameObject.tag == "tableObj" && !isGround && comboX < 3)
        {
            comboX = 0;
			if (!isDamage)
			if (jumpTimer <= 0) {
				isGround = true;
				jumpTimer = 0.3f;
			}

            clip = sounds[0];
            if(PlayerPrefs.GetInt("sound") == 0)
            source.PlayOneShot(clip, 1f);
        }
        if (collision.gameObject.tag == "tableObj" && comboX > 2) {
            maxY = transform.position.y - camY;
            isCam = true;
        }

        if (collision.gameObject.tag == "finish")
        {

            clip = sounds[2];
            if (PlayerPrefs.GetInt("sound") == 0)
                source.PlayOneShot(clip, 1f);
			PlayerPrefs.SetInt ("buffDamage", PlayerPrefs.GetInt ("buffDamage") + CS2.useDamages);
			PlayerPrefs.SetInt ("buffShield", PlayerPrefs.GetInt ("buffShield") + CS2.useShields);
            rc.enabled = false;
            Debug.Log("Победа");
            if (PlayerPrefs.GetInt("levelopen" + (jPars.levelid + 1)) == 0)
            {
                
                gSM.nEw = 1;
                gSM.level.text = (jPars.levelid + 2).ToString();
                PlayerPrefs.SetInt("levelopen" + (jPars.levelid + 1), 1);
                Debug.Log(PlayerPrefs.GetInt("levelopen" + jPars.levelid + 1));
            }
            
            if (count > PlayerPrefs.GetInt("CountForLevel" + PlayerPrefs.GetInt("levelSelect")))
            {
                PlayerPrefs.SetInt("CountForLevel" + PlayerPrefs.GetInt("levelSelect"), count);
            }
            gSM.wonScene();
        }

		if (collision.gameObject.tag == "enemy" && !isBuffs)
        {
			
				//shield.SetActive (false);
				if (isShield) {
					isShield = false;
					isGround = true;
			}else{
				if (collision.gameObject.GetComponent<Animator> () != null) {
					collision.gameObject.GetComponent<Animator> ().enabled = false;
				}
				rc = FindObjectOfType<rotateController> ();
				isStop = true;
				gSM.rC.enabled = false;
				gSM.isDead = true;
				//control.isKinematic = true;
				clip = sounds [1];
				if (PlayerPrefs.GetInt ("sound") == 0)
					source.PlayOneShot (clip, 1f);
				gSM.loseScene ();


				gSM.loseGame.GetComponentInChildren<Transform> ().Find ("maxScore").GetComponent<Text> ().text = PlayerPrefs.GetInt ("CountForLevel" + PlayerPrefs.GetInt ("levelSelect")).ToString ();
				if (count > PlayerPrefs.GetInt ("CountForLevel" + PlayerPrefs.GetInt ("levelSelect"))) {
					PlayerPrefs.SetInt ("CountForLevel" + PlayerPrefs.GetInt ("levelSelect"), count);
					gSM.loseGame.GetComponentInChildren<Transform> ().Find ("maxScore").GetComponent<Text> ().text = PlayerPrefs.GetInt ("CountForLevel" + PlayerPrefs.GetInt ("levelSelect")).ToString ();
				}
				//PlayerPrefs.SetInt ("buffDamage", PlayerPrefs.GetInt ("buffDamage") + CS2.useDamages);
				//PlayerPrefs.SetInt ("buffShield", PlayerPrefs.GetInt ("buffShield") + CS2.useShields);
				Debug.Log ("Мёртв");
			}
        }
    }

        public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "nextStage")
            isNullObject = true;
        else
            isNullObject = false;
    }

    public void OnTriggerExit(Collider other)
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
		

        if (other.gameObject.tag == "tableObj")
        {
			if (comboX > 2 || isDamage) {
                
                other.gameObject.GetComponentInParent<delObject>().delTime = 0.3f;

                
            }
            comboX = 0;
        }

        if (other.gameObject.tag == "nextStage")
        {
            gSM.nextStageCount += 1;
            comboX++;
            if (comboX >= 2)
            {
                speed += 0.1f;
            }
            else if (comboX == 0)
                speed = camSpeedNormal;



            maxY = transform.position.y - camY;
            isCam = true;
            if (comboX < 2)
            {
                count += 5;
            }
            else
            {
                count += 10;
            }
            gSM.playScene(count.ToString());
            other.gameObject.GetComponent<MeshCollider>().enabled = false;

        }


    }
	/*private bool IsGrounded(){
		Ray groundRay = new Ray (new Vector3 (controler.bounds.center.x, (controler.bounds.center.y - controler.bounds.extents.y + 0.2f), controler.bounds.center.z), Vector3.down);
		//Debug.DrawRay (groundRay.origin, groundRay.direction,Color.cyan,1f);
		return Physics.Raycast (groundRay, 0.2f + 0.1f);
	}*/
}