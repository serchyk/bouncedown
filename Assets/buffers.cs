﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TYPEBALL
{
	Shield = 0,
	DamageBall = 1,
}

public class buffers : MonoBehaviour {

	public TYPEBALL type;
	public int addCount;


	// Use this for initialization
	void OnTriggerEnter (Collider other) {
	
		if (other.tag == "player") {
			if (type == TYPEBALL.Shield) {
				Constants2 cs2 = FindObjectOfType<Constants2> ();
				cs2.useShields += addCount;
				//PlayerPrefs.SetInt ("buffShield", PlayerPrefs.GetInt("buffShield") + addCount);
				Destroy (gameObject);
			}

			if (type == TYPEBALL.DamageBall) {
				Constants2 cs2 = FindObjectOfType<Constants2> ();
				cs2.useDamages += addCount;
				//PlayerPrefs.SetInt ("buffDamage", PlayerPrefs.GetInt("buffDamage") + addCount);
				Destroy (gameObject);
			}
		}

	}
}
