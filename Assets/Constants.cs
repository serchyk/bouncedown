﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Constants : MonoBehaviour {

	public AllLevels al;

	public int shieldsCount;
	public int damagesCount;
	public int useShields;
	public int useDamages;
	int forCheckD;
	int forCheckS;

	public Slider sliderDamage;
	public Text textUseCountD;
	public Slider sliderShield;
	public Text textUseCountS;
	public GameObject buttonCD;
	public GameObject buttonCS;
	public GameObject buttonD;
	public GameObject buttonS;

	public void Start(){
		shieldsCount = PlayerPrefs.GetInt ("buffShield");
		damagesCount = PlayerPrefs.GetInt ("buffDamage");
		//useShields = PlayerPrefs.GetInt ("useShields");
		//useDamages = PlayerPrefs.GetInt ("useDamages");
		if(sliderDamage != null){
			//sliderDamage.minValue = useDamages;
			//sliderDamage.value = PlayerPrefs.GetInt ("useDamages");
			sliderDamage.maxValue = damagesCount;
		}
		if(sliderShield != null){
			//sliderShield.minValue = useShields;
			//sliderShield.value = PlayerPrefs.GetInt ("useShields");
			sliderShield.maxValue = shieldsCount;
		}
		forCheckD = PlayerPrefs.GetInt ("buffDamage");
		forCheckS = PlayerPrefs.GetInt ("buffShield");


	}

    public void Update()
    {
		
		shieldsCount = PlayerPrefs.GetInt ("buffShield");
		damagesCount = PlayerPrefs.GetInt ("buffDamage");

		if(sliderDamage != null){
			textUseCountD.text = useDamages.ToString() + "/" + forCheckD.ToString();

			useDamages = (int)sliderDamage.value;
		}
		if(sliderShield != null){
			textUseCountS.text = useShields.ToString() + "/" + (forCheckS - useShields).ToString() + "(" + forCheckS.ToString() + ")";

			useShields = (int)sliderShield.value;
		}
    }

	public void openAllLevels(){
		for (int i = 0; i < al.countLevel; i++) {
			PlayerPrefs.SetInt ("levelopen" + al.levels [i].id, 1);
			PlayerPrefs.SetInt ("levelLock" + al.levels [i], 1);
			al.levels [i].startGame = al.levels [i].gameObject.GetComponent<Button> ();
			al.levels [i].lockImage = al.levels [i].gameObject.GetComponentInChildren<Transform> ().Find ("Lock").GetComponent<Image> ();
			al.levels [i].lockImage.enabled = false;
			al.levels [i].startGame.enabled = true;
		}
	}

	public void acceptD(){
		//PlayerPrefs.SetInt ("buffDamage", PlayerPrefs.GetInt ("buffDamage") - useDamages);
		Debug.Log (PlayerPrefs.GetInt ("buffDamage"));
		//PlayerPrefs.SetInt ("useDamages", useDamages);
		buttonD.SetActive (false);
		buttonCD.SetActive (true);
	}

	public void cancelD(){
		//sliderDamage.value = 0;
		buttonD.SetActive (true);
		buttonCD.SetActive (false);
		//PlayerPrefs.SetInt ("buffDamage", PlayerPrefs.GetInt ("buffDamage") + useDamages);
		Debug.Log (PlayerPrefs.GetInt ("buffDamage"));
	}


	public void acceptS(){
		//PlayerPrefs.SetInt ("buffShield", PlayerPrefs.GetInt ("buffShield") - useShields);
		Debug.Log (PlayerPrefs.GetInt ("buffShield"));
		PlayerPrefs.SetInt ("useShields", useShields);
		PlayerPrefs.SetInt ("MENU", 1);
		buttonS.SetActive (false);
		buttonCS.SetActive (true);
		sliderShield.value = useShields;
	}

	public void cancelS(){
		//sliderShield.value = 0;
		buttonS.SetActive (true);
		buttonCS.SetActive (false);
		//PlayerPrefs.SetInt ("buffShield", PlayerPrefs.GetInt ("buffShield") + useShields);
		Debug.Log (PlayerPrefs.GetInt ("buffShield"));
	}

	public void DebugBuffs(){
		PlayerPrefs.SetInt ("useShields",0);
		PlayerPrefs.SetInt ("useDamages",0);
		PlayerPrefs.SetInt ("buffShield",0);
		PlayerPrefs.SetInt ("buffDamage",0);
	}
}