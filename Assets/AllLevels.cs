﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Score
{
	public float Min;
	public float Max;
}

public class AllLevels : MonoBehaviour {

	public Score score; 
	public GameObject prefabLevel;
    public List<LevelInfo> levels;
    int countFiles;
    int countLevels;
    public List<string> levelsName;
    public int countLevel;
	string nameLevel;

    public void Start()
    {
        //Path.GetFullPath(Application.streamingAssetsPath).Length
        for (int i = 0; i < countLevel; i++)
        {
            //countLevels = Path.GetFullPath(Application.streamingAssetsPath + "/"+"level" + i + ".json").Length;//Directory.GetFiles(Application.streamingAssetsPath, "level" + i + ".json").Length;
                levelsName.Add("Level" + i);
        }
		PlayerPrefs.SetInt ("levelCount", countLevel);
			
        //yield return data;

		nameLevel = prefabLevel.name;

        for (int i = 0; i < levelsName.Count; i++)
        {
			GameObject prefabLevel = Instantiate(Resources.Load<GameObject> (nameLevel));
            prefabLevel.name = (levelsName[i]);
            prefabLevel.transform.SetParent(GameObject.Find("container").transform);
            prefabLevel.GetComponent<Image>().sprite = Resources.Load<Sprite>("level_image/fon");
            
			if (PlayerPrefs.GetInt("CountForLevel" + i) >= score.Min)
                 prefabLevel.GetComponent<LevelInfo>().star[0].sprite = Resources.Load<Sprite>("level_image/star");
            else
                 prefabLevel.GetComponent<LevelInfo>().star[0].sprite = Resources.Load<Sprite>("level_image/un_star");

			if (PlayerPrefs.GetInt("CountForLevel" + i) >= score.Max - score.Min)
                prefabLevel.GetComponent<LevelInfo>().star[1].sprite = Resources.Load<Sprite>("level_image/star");
            else
                prefabLevel.GetComponent<LevelInfo>().star[1].sprite = Resources.Load<Sprite>("level_image/un_star");

			if (PlayerPrefs.GetInt("CountForLevel" + i) >= score.Max)
                prefabLevel.GetComponent<LevelInfo>().star[2].sprite = Resources.Load<Sprite>("level_image/star");
            else
                prefabLevel.GetComponent<LevelInfo>().star[2].sprite = Resources.Load<Sprite>("level_image/un_star");

            levels.Add(GameObject.Find(levelsName[i]).GetComponent<LevelInfo>());
            //levels[i].gameObject.GetComponentInChildren<Transform>().Find("Name").GetComponent<Image>().color = Color.black;
            levels[i].id = i;
            
            prefabLevel.GetComponent<LevelInfo>().nameLevel.text = (prefabLevel.GetComponent<LevelInfo>().id + 1).ToString();
            if (i == 0 || i == 1)
            {
                PlayerPrefs.SetInt("levelopen" + levels[i].id, 1);
                PlayerPrefs.SetInt("levelLock" + levels[i], 1);
                levels[i].startGame = levels[i].gameObject.GetComponent<Button>();
                levels[i].lockImage = levels[i].gameObject.GetComponentInChildren<Transform>().Find("Lock").GetComponent<Image>();
                levels[i].lockImage.enabled = false;
                levels[i].startGame.enabled = true;
            }

            levels[i].count = PlayerPrefs.GetInt("CountForLevel" + i);
            if (i != 0)
            {
                levels[i].startGame = levels[i].gameObject.GetComponent<Button>();
                levels[i].lockImage = levels[i].gameObject.GetComponentInChildren<Transform>().Find("Lock").GetComponent<Image>();
                levels[i].previuLevel = levels[i - 1].gameObject;
                prefabLevel.GetComponent<LevelInfo>().lockImage.sprite = Resources.Load<Sprite>("level_image/lock");
                if (PlayerPrefs.GetInt("levelopen" + levels[i].id) == 1)
                {
                    PlayerPrefs.SetInt("levelLock" + levels[i], 1);
                    levels[i].lockImage.enabled = false;
                    levels[i].startGame.enabled = true;
                }
                else
                    PlayerPrefs.SetInt("levelopen" + levels[i].id, 0);
            }

            if (PlayerPrefs.GetInt("levelopen" + levels[i].id) == 1) {
                levels[i].Unlock = true;
            }
            Debug.Log(PlayerPrefs.GetInt("levelopen" + levels[i].id));
        }
    }
}
